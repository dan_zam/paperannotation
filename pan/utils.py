import tempfile, os, subprocess, re

class FixedAttributeClass(object):
    __isfrozen = False

    def __setattr__(self, key, value):
        if self.__isfrozen and not hasattr(self, key):
            raise TypeError( "%r is a frozen class" % self )
        object.__setattr__(self, key, value)

    def _freeze(self):
        self.__isfrozen = True

# https://www.geeksforgeeks.org/print-colors-python-terminal/
class fs:
    reset='\033[0m'
    bold='\033[01m'
    disable='\033[02m'
    underline='\033[04m'
    reverse='\033[07m'
    strikethrough='\033[09m'
    invisible='\033[08m'
class fg:
    black='\033[30m'
    red='\033[31m'
    green='\033[32m'
    orange='\033[33m'
    blue='\033[34m'
    purple='\033[35m'
    cyan='\033[36m'
    lightgrey='\033[37m'
    darkgrey='\033[90m'
    lightred='\033[91m'
    lightgreen='\033[92m'
    yellow='\033[93m'
    lightblue='\033[94m'
    pink='\033[95m'
    lightcyan='\033[96m'
class bg:
    black='\033[40m'
    red='\033[41m'
    green='\033[42m'
    orange='\033[43m'
    blue='\033[44m'
    purple='\033[45m'
    cyan='\033[46m'
    lightgrey='\033[47m'


# def read_from_editor(initial_message=""):
#     # https://stackoverflow.com/questions/6309587/call-up-an-editor-vim-from-a-python-scripts
#
#     add_as_comment = lambda msg: "### > {} < ###".format(msg).encode()
#
#     EDITOR = os.environ.get('EDITOR','vim') #that easy!
#
#     with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
#         tf.write(b"\n")
#         tf.write(add_as_comment(initial_message))
#         tf.flush()
#         subprocess.call([EDITOR, tf.name])
#
#         # do the parsing with `tf` using regular File operations.
#         # for instance:
#         tf.seek(0)
#         edited_message = tf.read().decode("utf-8")
#
#     edited_message = re.sub(r"\#\#\# \>[\w\s\d]*\< \#\#\#", r"", edited_message)
#     edited_message = re.sub(r"\A[\n]*", r"", edited_message)
#     edited_message = re.sub(r"[\n]*\Z", r"", edited_message)
#     return edited_message

def editor_form(form):
    # https://stackoverflow.com/questions/6309587/call-up-an-editor-vim-from-a-python-scripts

    add_as_comment = lambda msg: "\n### > {} < ###\n".format(msg).encode()
    add_as_field_header = lambda msg: "[[[_{}_]]]\n".format(msg).encode()
    add_as_field_entry = lambda msg: "{}\n".format(msg).encode()

    EDITOR = os.environ.get('EDITOR','vim') #that easy!

    with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
        for type, msg in form:
            if type == "comment":
                tf.write(add_as_comment(msg))
            elif type == "field":
                tf.write(add_as_field_header(msg[0]))
                tf.write(add_as_field_entry(msg[1]))
            else:
                raise ValueError("Entry of type {} not recognized.".format(type))
        tf.flush()
        subprocess.call([EDITOR, tf.name])
        # do the parsing with `tf` using regular File operations.
        # for instance:
        tf.seek(0)
        edited_message = tf.read().decode("utf-8")

    # print("-----edited_message--")
    # print(edited_message)
    # print("------------edited_message------------")
    #
    # edited_message = '''
    # provassero anche gli altri
    # ### > a comment < ###
    # [[[_field 1_]]]
    # inputo for fiela 1
    # [[[_field 2_]]]
    # two lines
    # for filed 2
    #
    # ### > comment 2 < ###
    # [[[_field 3_]]]i inline for the third
    # '''

    # Parse edited message:
    edited_message = re.sub(r"\#\#\# \>.*\< \#\#\#", r"", edited_message)
    edited_message = re.sub(r"[\n\s]*(\[\[\[_[\w\s\d]*_\]\]\])[\n\s]*", r"|||sep|||\1", edited_message)
    edited_message = re.sub(r"\A[\n\s]*", r"", edited_message)
    edited_message = re.sub(r"[\n\s]*\Z", r"", edited_message)
    
    row_input = edited_message.split("|||sep|||")
    input_form = []

    res = re.findall(r"\[\[\[_([\w\s\d]*)_\]\]\](.*)", row_input[0])
    if len(res) == 0:
        input_form.append((None, row_input[0]))
        row_input.pop(0)

    for r in row_input:
        [(field_name, field_val, _)] = re.findall(r"\[\[\[_([\w\s\d]*)_\]\]\]((.*\n*)*)", r)
        input_form.append((field_name, field_val))
        
    # print(input_form)
    return input_form

def read_from_editor(initial_message=""):
    form = [("comment", initial_message)]
    field, val = editor_form(form)[0]
    assert field is None
    return val
    
def debug():
    form = [("comment", "a comment"),
            ("field", ["field 1", ""]),
            ("field", ["field 2", ""]),
            ("comment", "comment 2"),
            ("field", ["field 3", ""])]

    print(editor_form(form))
    print(read_from_editor())


YES, NO, ALL, QUIT = "y", "n", "a", "q" 

def yes_no_all_quit_input(toall=False, default=1):

    if not toall:
        assert default < 2

    available = [YES, NO]
    if toall:
        available += [ALL, QUIT]


    s = "{}[y]es{} / {}[n]o{}"
    if toall:
        s += " / {}yes[a]ll{} / {}[q]uit{}"
    s += ": "
    # sty = [""]*2*default + ["--","--"] + [""] * 2 * ((3 if toall else 1) - default) 
    sty = [""]*2*default + [fs.bold, fs.reset] + [""] * 2 * ((3 if toall else 1) - default) 

    # return s.format(*sty)
    res = input(s.format(*sty))
    if res not in available:
        res = available[default]

    return res


if __name__ == "__main__":
    debug()