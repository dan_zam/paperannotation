from markdown import markdown
import subprocess
import os
from datetime import datetime

import pan

def google_scholar_query_paper(query):
    import urllib
    return "https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q={}".format(urllib.parse.quote_plus(query))

def google_scholar_num_citations(url):
    raise NotImplementedError("# TODO use the Google Scholar API")
    import requests
    response = requests.get(url.replace("%20", "+"))
    import re
    c = re.findall(response.text, r"Cited\sby\s([\d]+)")
    return "..".join(c)

def arxiv_query_paper(query):
    return "https://arxiv.org/search/?query={}&searchtype=title&abstracts=hide&order=-announced_date_first&size=50".format(query.replace(" ", "+"))

def save_paper_locations(paper_locations, filename):
    import json
    with open(filename, "w") as json_file:
        json.dump(paper_locations, json_file)

def load_paper_locations(filename):
    import os, json
    if not os.path.isfile(filename):
        return None
    with open(filename, "r") as json_file:
        paper_locations = json.load(json_file)
    return paper_locations

class Report(object):
    def __init__(self, filename, path, paper_locations=None):
        self.filename = filename
        self.path = path
        self.paper_locations = paper_locations
    
    def generate(self, db):
        raise NotImplemented
    

class MarkDownReport(Report):

    def get_paper_string(self, paper, with_num_citations=False):
        ps = ""
        ps += '<a name="{}"></a>  \n ### {}  \n'.format(paper.id, paper.title) 
        ps += "_{}_   \n".format(paper.author)
        ps += "{}   \n".format(paper.venue)

        # boxes
        ps += "|`{}`|".format(paper.id)
        ps += "`{}`|".format(paper.last_update)
        ps += f'<a href="javascript:void(0);" onclick=\'togglebibtex("bibtex-{ paper.id }")\'> `bibtex` </a>|'
        if paper.doi != pan.NOT_SET:
            ps += "[`doi`]({})|".format(paper.doi)
        ps += "`{}`|".format(paper.keywords)

        url = google_scholar_query_paper(paper.title)
        cited = ""
        if with_num_citations:
            cited = " " + google_scholar_num_citations(url)
        # ps += "[`GoogleScholar{}`]({})|".format(cited, url)
        # ps += "[`GoogleScholar{}`]({}){}|".format(cited, url, '{:target="_blank"}')
        ps += '<a href="{}" target="_blank">`GoogleScholar`{}</a>|'.format(url, cited)
        if paper.link_preprint != pan.NOT_SET:
            ps += "[`preprint/local?`]({})|".format(paper.link_preprint)
        if paper.arxiv_id != pan.NOT_SET:
            # ps += "[`arXiv`](https://arxiv.org/abs/{})|".format(paper.arxiv_id)
            ps += '<a href="https://arxiv.org/abs/{}" target="_blank">`arXiv`</a>|'.format(paper.arxiv_id)
        else:
            # ps += "[`arXiv?`]({})|".format(arxiv_query_paper(paper.title))
            ps += '<a href="{}" target="_blank">`arXiv query`</a>|'.format(arxiv_query_paper(paper.title))
        if self.paper_locations is not None:
            if paper.id in self.paper_locations:
                ps += '<a href="{}" target="_blank">`local pdf ({}%)`</a>|'.format(*self.paper_locations[paper.id])
        # ps += '<a="{}" target="_blank">`GoogleScholar`</a>|'.format(google_scholar_query_paper(paper.title))
        # ps += "[`GoogleScholar`]({}){}|".format(google_scholar_query_paper(paper.title), '{:target="_blank"}')
        ps += f'<pre class="bibtex-block" id="bibtex-{ paper.id }" style="display: none"><code> {paper.bibtex} </code></pre>'
        ps += "\n\n"
        comment = self.parse_links(paper.comment)
        ps += "{}".format(comment)
        ps += "\n\n\n"
        return ps
    
    def generate(self, db, path=None, filename=None, paper_locations=None):
        if path is None:
            path = self.path
        if filename is None:
            filename = self.filename
        if paper_locations is None:
            paper_locations = self.paper_locations

        abs_file = os.path.join(path, filename)
        print("creating MarkDown report to {}".format(abs_file))
        with open(abs_file, "w") as file:
            file.write(f'# Pan \nLast update: {datetime.now().strftime("%d/%m/%Y")}\n')
            for paper in db.get_all()[::-1]:
                # file.write('<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>')
                file.write(self.get_paper_string(paper))
                file.flush()

    def parse_links(self, text):
        import re
        # find arxiv bibtex ids of the form "#w...w0000w...w" except when they are preceded or followed by rounded brackets
        return re.sub(r"([^\(]+)\#([\w]+\d\d\d\d[\w\d]+)([^\w\d\)]+)", r"\1[\2](#\2)\3", text)

class SimpleHTMLReport(MarkDownReport):

    def get_paper_string(self, paper):
        return markdown(super(SimpleHTMLReport, self).get_paper_string(paper))

class MarkDown2HTMLReport(MarkDownReport):

    def __init__(self,
                 add_search_functionality=True,
                 add_togglebibtex=True,
                 *args, **kwargs):
        super(MarkDown2HTMLReport, self).__init__(*args, **kwargs)
        if self.filename[-5:] == ".html":
            self.filename = self.filename[:-5]
        if self.filename[-3:] == ".md":
            self.filename = self.filename[:-3]
        self.add_search_functionality = add_search_functionality
        self.add_togglebibtex = add_togglebibtex

    def generate(self, db):
        input_path = os.path.join(self.path, "input")
        output_path = os.path.join(self.path, "output")
        assert os.path.isdir(input_path), "path {} not found".format(input_path)
        # if not os.path.isdir(input_path):
        #     os.path.mkdir(input_path)
        assert os.path.isdir(output_path), "path {} not found".format(output_path)
        filename = self.filename + ".md"
        super(MarkDown2HTMLReport,self).generate(db=db, path=input_path, filename=filename)
        cmd = "generate-md --layout github --input {} --output {}".format(input_path, output_path)
        print("Generating HTML report at {} with command\n{}".format(output_path, cmd))
        subprocess.call(cmd.split())
        self.add_javascript(search_functionality=self.add_search_functionality, togglebibtex=self.add_togglebibtex)

    def add_javascript(self, search_functionality, togglebibtex):
        if not search_functionality and not togglebibtex:
            return
        output_path = os.path.join(self.path, "output")
        file = os.path.join(output_path, self.filename + ".html")
        assert os.path.isfile(file)
        cmd = "cp {} {}.bak".format(file, file)
        subprocess.call(cmd.split())

        from bs4 import BeautifulSoup
        with open(file + ".bak", "r") as fin:
            soup = BeautifulSoup(fin, 'html.parser')

            if search_functionality:
                tag_help = soup.new_tag("p")
                tag_help["id"] = "help"
                tag_help.string = "Help:"
                tag_help.append(soup.new_tag("br"))

                tag_help_actual = soup.new_tag("small")
                tag_help_actual.string = " Search with query like "
                tag_help.append(tag_help_actual)

                tmp = soup.new_tag("code")
                tmp.string = "...pan.html?s=graph"
                tag_help_actual.append(tmp)

                # tag_help_actual.append(soup.new_tag("br"))

                tag_help_actual.append(". Move to the next by pressing ")

                tmp = soup.new_tag("code")
                tmp.string = "F"
                tag_help_actual.append(tmp)

                tag_help_actual.append(" or press ")

                tmp = soup.new_tag("code")
                tmp.string = "ctrl+F"
                tag_help_actual.append(tmp)

                tag_help_actual.append(" to navigate as usual.")

                soup.body.find_all('h1', id="pan")[0].insert_after(tag_help)

                soup.body["onkeypress"] = "uniCharCode(event)"

            tag_script_fun = soup.new_tag("script")
            tag_script_fun["type"] = "text/javascript"

            tag_script_fun.string = ""
            if search_functionality:
                tag_script_fun.string += """
const query_word = new URLSearchParams(window.location.search).get('s');
function findNext(){
    window.find(query_word);  
}
function uniCharCode(event) {
  var char = event.which || event.keyCode;
  if (event.keyCode == 102) {
    findNext();
  }
}
                """
            if togglebibtex:
                tag_script_fun.string += """
function togglebibtex(paperid) {
  var x = document.getElementById(paperid);
  if (x.style.display === "none") {
    x.style.display = "block";
    x.innerHTML = x.innerHTML.replace(/, ([a-z]+)={/g,\',\\n  $1={\');
  } else {
    x.style.display = "none";
  }
}
                   """

        soup.body.insert_before(tag_script_fun)

        tag_script_call = soup.new_tag("script")
        tag_script_call["type"] = "text/javascript"
        tag_script_call.string = ""

        if search_functionality:
            tag_script_call.string += """
if (query_word != null){
  findNext()
}
            """
        soup.body.insert_after(tag_script_call)

        with open(file, "w") as fout:
            fout.write(str(soup))

        cmd = "rm {}.bak".format(file)
        subprocess.call(cmd.split())



class MarkDown2PDFReport(MarkDownReport):

    def __init__(self, *args, **kwargs):
        super(MarkDown2HTMLReport, self).__init__(*args, **kwargs)
        if self.filename[-5:] == ".html":
            self.filename = self.filename[:-5]
        if self.filename[-3:] == ".md":
            self.filename = self.filename[:-3]

    def generate(self, db):
        input_path = os.path.join(self.path, "input")
        output_path = os.path.join(self.path, "output")
        assert os.path.isdir(input_path), "path {} not found".format(input_path)
        # if not os.path.isdir(input_path):
        #     os.path.mkdir(input_path)
        assert os.path.isdir(output_path), "path {} not found".format(output_path)
        filename = self.filename + ".md"
        super(MarkDown2HTMLReport,self).generate(db=db, path=input_path, filename=filename)
        cmd = "markdown-pdf --out {} {}".format(output_path, input_path)
        print("Generating PDF report at {} with command\n{}".format(output_path, cmd))
        raise NotImplementedError()
        subprocess.call(cmd.split())

# if __name__ == "__main__":

def debug():
    mk = MarkDownReport(path=".",filename="test_report.md")
    db = pan.PaperDataBase()

    mk.generate(db)



