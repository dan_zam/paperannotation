"""
This files handles papers:
- Class `Paper` implements single papers;
- Class `PaperDataBase` implements a set of papers.
"""
import re
from copy import deepcopy
from datetime import datetime
import json
import textdistance

from .utils import FixedAttributeClass

NOT_SET = "n.s."
DB_DEFAULT_JSON = "pan_data.json"
LOC_PAPER_DEFAULT_JSON = "pan_local_paper_locations.json"

EMPTY_DATA_JSON = b'{"papers":[]}'


def parse_latex(latex):
    res = latex
    res = res.replace("\`o", "ò")
    res = res.replace("{\`a}", "à")
    res = re.sub(r"{([\w\s\d]*)}", r"\1", res)
    return res


class Paper(FixedAttributeClass):
    """
    This class extends `FixedAttributeClass` in order to force a predefined
    set of attributes.
    """
    
    def __init__(self, paper_id, bibtex, comment, **kwargs):
        self.id = paper_id
        self.bibtex = clean_bibtex(bibtex)
        self.comment = comment
        self.date_creation = kwargs.pop("date_creation", datetime.now().strftime("%Y-%m-%d"))
        self.last_update =   kwargs.pop("last_update",   self.date_creation)
        self.keywords =      kwargs.pop("keywords",      NOT_SET)
        self.doi =           kwargs.pop("doi",           NOT_SET)
        self.official_url =  kwargs.pop("official_url",
                                        kwargs.pop("_official_url", NOT_SET))
        self.link_preprint = kwargs.pop("link_preprint", NOT_SET)
        self.repository =    kwargs.pop("repository",    NOT_SET)
        self.experiment_results = kwargs.pop("experiment_results", NOT_SET)

        self.manualmonth = NOT_SET
        self.order = kwargs.pop("order", kwargs.pop("_order", 0))

        self._freeze()

    @property
    def author(self):
        return get_bibtex_field(field="author", bibtex=self.bibtex)

    @property
    def short_author(self):
        return re.sub(r"([\w]+),\s[\w]+\s?[\w]*\s(and)?", r"\1, ", parse_latex(self.author) + " and")[:-2]

    @property
    def title(self):
        return get_bibtex_field(field="title", bibtex=self.bibtex)

    @property
    def year(self):
        return get_bibtex_field(field="year", bibtex=self.bibtex)

    @property
    def month(self):
        try:
            bibmonth = get_bibtex_field(field="month", bibtex=self.bibtex)
            months = [["jan"], 
                      ["feb"], 
                      ["mar"], 
                      ["apr"], 
                      ["may"], 
                      ["jun"], 
                      ["jul"], 
                      ["aug"], 
                      ["sep"], 
                      ["oct"], 
                      ["nov"], 
                      ["dec"]]
            for e, month_name in enumerate(months):
                for mn_ in month_name:
                    if mn_ in bibmonth.lower():
                        return e + 1 
        except KeyError:
            pass

        if self.manualmonth is not NOT_SET:
            return self.manualmonth

        return 0

    @property
    def order(self):
        return self._order if self._order != 0 else self.month
        # return f"{o:011.5f}"

    @order.setter
    def order(self, value):
        try:
            self._order = float(value)
        except:
            pass
            print(f"failed to read {value} as float")

    @property
    def arxiv_id(self):
        arxiv_ids = re.findall(r"arxiv:(\d\d\d\d\.\d\d\d\d\d)", self.bibtex.lower())
        arxiv_ids += re.findall(r"arxiv\.org\/abs\/(\d\d\d\d\.\d\d\d\d\d)", self.bibtex.lower())
        arxiv_ids += re.findall(r"(\d\d\d\d\.\d\d\d\d\d)", self.link_preprint)
        arxiv_ids = list(set(arxiv_ids))
        assert len(arxiv_ids) < 2, f'Multiple arXiv ids in {self.id}: {" ".join(arxiv_ids)}'
        return NOT_SET if len(arxiv_ids)==0 else arxiv_ids[0]

    @property
    def official_url(self):
        if self._official_url != NOT_SET:
            return self._official_url
        else:
            try:
                return get_bibtex_field(field="url", bibtex=self.bibtex)
            except KeyError:
                return NOT_SET

    @official_url.setter
    def official_url(self, value):
        self._official_url = value

    @property
    def venue(self):
        venue_fileds = ["journal", "booktitle", "publisher"]
        for field in venue_fileds:
            try:
                venue = get_bibtex_field(field=field, bibtex=self.bibtex)
                return venue
            except KeyError:
                pass
        raise KeyError("No venue key among {{{}}} found for paper {}".format(",".join(venue_fileds), self.id, self))

    @property
    def short_venue(self):
        venue = self.venue.lower()
        if "international conference on machine learning".lower() in venue:
            return "ICML"
        if "international conference on learning representations".lower() in venue:
            return "ICLR"
        if "Neural Information Processing System (NeurIPS), Graph Representation Learning Workshop".lower() in venue:
            return "GRL @ NeurIPS"
        if "Neural information processing system".lower() in venue:
            return "NeurIPS"
        if "joint conference on neural network".lower() in venue:
            return "IEEE IJCNN"
        if "symposium series on computational intelligence".lower() in venue:
            return "IEEE SSCI"
        if "Transactions on Neural Networks and Learning Systems".lower() in venue:
            return "IEEE TNNLS"
        if "Transactions on Signal Processing".lower() in venue:
            return "IEEE TSP"
        if "European Symposium on Artificial Neural Networks, Computational Intelligence and Machine Learning".lower() in venue:
            return "ESANN"
        if "European Conference on Machine Learning and Knowledge Discovery in Databases".lower() in venue:
            return "ECML PKDD"

        if "@phdthesis" in self.bibtex:
            return "PhD Thesis"

        return NOT_SET

    @property
    def publication_type(self):
        if "inproceeding" in self.bibtex:
            return "conference"
        elif "article" in self.bibtex:
            return "journal"
        elif "phdthesis" in self.bibtex:
            return "thesis"
        else:
            return "other"

    def as_json_entry(self):
        dic = deepcopy(self.__dict__)
        dic.pop("_FixedAttributeClass__isfrozen")
        return dic

    @classmethod
    def from_json_entry(cls, json_dict):
        d = deepcopy(json_dict)
        return Paper(paper_id=d.pop("id"),
                     bibtex=d.pop("bibtex"),
                     comment=d.pop("comment"),
                     **d)

    def dumps_comment_html(self):
        import markdown
        cmt = markdown.markdown( self.comment)
        # cmt = re.sub(r"\n", "<br/>", cmt)
        cmt = re.sub(r"\n-", "<br/>-", cmt)
        cmt = re.sub(r"\n -", "<br/> -", cmt)
        cmt = re.sub(r"\n+", "<br/>+", cmt)
        cmt = re.sub(r"\n +", "<br/> +", cmt)
        cmt = re.sub(r"\n(\d)\.", r"<br/>\1 ", cmt)
        cmt = re.sub(r"\n", "", cmt)

        return cmt

    def dumps_extended_bibtex_for_zotero(self, additional_fields):
        add_fields = ""
        extra_field = ""
        for field, paper_arg in additional_fields.items():
            if isinstance(paper_arg, dict):
                for name, pa in paper_arg.items():
                    val = self.__dict__[pa]
                    if val not in [NOT_SET, ""]:
                        extra_field += f"{name}: {val}\n "
            else:
                if paper_arg == "title":
                    val = self.title if self.comment != "" else ""
                else:
                    val = self.__dict__[paper_arg]
                if val not in [NOT_SET, ""]:
                    add_fields += f"{field}={{{val}}}, "
        if extra_field == "":
            pass
        else:
            add_fields += f"extra={{{extra_field}}}, "

        i = 1
        while self.bibtex[-i] != "}":
            i += 1
        i += 1
        while self.bibtex[-i] not in ["}", ","]:
            i += 1
        assert i < 10
        if self.bibtex[-i] == ",":
            bibtex_ = self.bibtex[:-i+1] + " " + add_fields + "}"
        elif self.bibtex[-i] == "}":
            bibtex_ = self.bibtex[:-i+1] + ", " + add_fields + "}"
        else:
            raise ValueError()
        bibtex_ = re.sub(r"\n", "\\\\par", bibtex_)
        return bibtex_

    def dumps_bibtex(self, additional_fields={}):
        bibtex_ = self.format_additiona_fields(additional_fields)
        bib = re.sub(r", ([a-z]+)={", r",\n  \1={", bibtex_)
        out = re.sub("},? }", r"},\n}", bib)
        return out

    def dumps_yaml(self):
        yaml_str = "\n    id: " + self.id
        yaml_str += "\n    title: \"" + parse_latex(self.title) + "\""
        yaml_str += "\n    author: \"" + parse_latex(self.author) + "\""
        yaml_str += "\n    short_author: \"" + parse_latex(self.short_author) + "\""
        yaml_str += "\n    year: " + self.year
        yaml_str += "\n    bibtex: \"" + self.bibtex.replace("\\","\\\\") + "\""

        if self.doi != NOT_SET:  yaml_str += "\n    doi: \"" + self.doi + "\""
        if self.arxiv_id != NOT_SET:  yaml_str += "\n    arxiv_id: \"" + self.arxiv_id + "\""
        if self.short_venue != NOT_SET:  yaml_str += "\n    short_venue: \"" + self.short_venue + "\""

        yaml_str += "\n    type: " + self.publication_type

        if self.official_url != NOT_SET:
            yaml_str += "\n    url: \"" + self.official_url + "\""
        elif self.link_preprint != NOT_SET:
            yaml_str += "\n    url: \"" + self.link_preprint + "\""

        if self.repository != NOT_SET:
            if "github.com" in self.repository:
                yaml_str += "\n    github: \"" + self.repository + "\""
            elif "gitlab.com" in self.repository:
                yaml_str += "\n    gitlab: \"" + self.repository + "\""
            else:
                yaml_str += "\n    repository: \"" + self.repository + "\""

        if self.experiment_results != NOT_SET:  yaml_str += "\n    experiments: \"" + self.experiment_results + "\""

        return yaml_str

    def to_string(self, color=False, compact=True, comment=False):

        from pan.utils import fs, fg

        # ID
        pstr = "["
        if color:  pstr += fs.bold
        pstr += self.id
        if color:  pstr += fs.reset
        pstr += "] "

        # venue
        if not compact:  pstr += self.venue + "\n  "

        # authors
        if color:  pstr += fg.yellow
        pstr += self.author + ", "
        if not compact:  pstr += "\n  "

        # title
        if color:  pstr += fg.lightred
        pstr += self.title
        if color:  pstr += fs.reset

        if not compact and comment:
            pstr += "\n\n"
            if color:  pstr += fg.lightgrey
            pstr += self.comment
            if color:  pstr += fs.reset

        return pstr

    def __repr__(self):
        return self.to_string(color=False, compact=True)

class PaperDataBase(object):
    _the_instance = None

    def __init__(self, filename=DB_DEFAULT_JSON):
        self.filename = filename

    def fetch(self):
        if self._the_instance is None:
            with open(self.filename) as json_file:
                self._the_instance = json.load(json_file)

    def init_empty(self):
        import os
        if os.path.isfile(self.filename):
            raise FileExistsError
        with open(self.filename, "wb") as json_file:
            json_file.write(EMPTY_DATA_JSON)
        return self

    @property
    def papers(self):
        self.fetch()
        return self._the_instance["papers"]
    
    def _get(self, paper_id):
        for idx, paper_dict in enumerate(self.papers):
            if paper_dict["id"] == paper_id:
                return idx, Paper.from_json_entry(paper_dict)
        return -1, None

    def get(self, paper_id):
        _, paper = self._get(paper_id)
        return paper

    def get_all(self, sort=None, inverse=False):
        papers = [Paper.from_json_entry(paper_dict) for paper_dict in self.papers]
        if sort is not None:
            papers = sorted(papers, key=lambda p: getattr(p, sort))
        if inverse:
            return papers[::-1]
        else:
            return papers

    def get_from_latex_aux(self, file_aux):
        with open(file_aux) as f:
            content = f.read()
            paper_ids = []
            # this should work for biblatex
            for p in re.findall(r"\\citation\{([\w\d\,]*)\}", content):
                paper_ids += p.split((","))
            # this should work for biber
            for p in re.findall(r"abx\@aux\@cite\{([\w\d\,]*)\}", content):
                paper_ids += p.split((","))
            paper_ids = set(paper_ids)
        paper_ids = sorted(paper_ids)
        papers = []
        papers_notfound = []
        for paper_id in paper_ids:
            res, paper = self._get(paper_id)
            if res < 0:
                papers_notfound.append(paper_id)
            else:
                papers.append(paper)
        return papers, papers_notfound

    def get_from_creationdate(self, from_date, to_date):
        papers_within_pediod = []
        for paper in self.papers:
            date = datetime.strptime(paper["date_creation"], '%Y-%m-%d').date()
            if from_date <= date <= to_date:
                papers_within_pediod.append(Paper.from_json_entry(paper))

        return papers_within_pediod

    def get_from_author(self, author):
        papers_from_author = []
        for paper in self.papers:
            if author in get_bibtex_field(field="author", bibtex=paper["bibtex"]).lower():
                papers_from_author.append(Paper.from_json_entry(paper))
            # paper = Paper.from_json_entry(paper)
            # if author in paper.author.lower():
            #     papers_from_author.append(paper)
        return papers_from_author

    def has_paper(self, new_paper):
        idx, _ = self._get(new_paper.id)
        return idx >= 0

    def add(self, papers):
        if not isinstance(papers, list):
            papers = [papers]
        for paper in papers:
            if self.has_paper(paper):
                _, old_paper = self._get(paper.id)
                msg = "Trying to add paper \n{}\n to database, however paper \n{}\n is already present".format(paper, old_paper)
                print(msg)
                raise KeyError(msg)
            self.papers.append(paper.as_json_entry())
        return self
    
    def save(self, filename=None):
        if filename is None:
            filename = self.filename

        with open(filename, "w") as json_file:
            json.dump(self._the_instance, json_file)

    def update(self, paper_id, edits):
        paper_idx, paper = self._get(paper_id)
        new_id = None
        for e in edits:
            if e[0] == "id":
                new_id = e[1]

        if new_id is not None:
            new_bibtex = paper.bibtex
            idx = -1
            for i in range(len(edits)):
                if edits[i][0] == "bibtex":
                    new_bibtex = edits[i][1]
                    idx = i
                    break
            new_bibtex = set_id_in_bibtex(new_id, new_bibtex)
            if idx > 0:
                edits.pop(idx)
            edits.append(("bibtex", new_bibtex))

        for e in edits:
            paper.__setattr__(e[0], e[1])
        
        self.papers[paper_idx] = paper.as_json_entry()

    def _query_field(self, query, field, num_best):
        query = query.lower()
        ret = []
        best_ids = [None]*num_best
        best_hds = [1e10]*num_best
        for paper_dict in self.papers:
            paper_field = paper_dict.get(field, Paper.from_json_entry(paper_dict).__getattribute__(field))
            if paper_field is None:
                raise AttributeError(f'Paper {paper_dict["id"]} has no field {field}.')
            paper_field = paper_field.lower()
            if query in paper_field:
                ret.append(paper_dict["id"])
            else:
                hd = textdistance.hamming.distance(query, paper_field)/len(paper_field)
                ins = -1
                for i in range(num_best):
                    if hd < best_hds[-(i+1)]:
                        ins = i
                if ins >= 0:
                    best_ids.insert(-(i+1), paper_dict["id"])
                    best_hds.insert(-(i+1), hd)
                    best_ids.pop()
                    best_hds.pop()

        return ret + best_ids

    def query_id(self, query, num_best=3):
        return self._query_field(query=query, field="id", num_best=num_best)

    def query_author(self, query, num_best=3):
        return self._query_field(query=query, field="author", num_best=num_best)

    def query_title(self, query, num_best=3):
        return self._query_field(query=query, field="title", num_best=num_best)

    def get_similar_papers(self, query_paper, num_max=4):
        # equal_id = self.get(paper_id)
        # sim_id = self.get_similar_id(paper_id, num_max=3)
        # sim_title = self.get_similar_title(paper_id, num_max=3)
        # sim_authors = self.get_similar_authors(paper_id, num_max=3)
        assert num_max > 0
        similar_papers = []
        found, equal_id = self._get(query_paper.id)
        if found:
            similar_papers.append(equal_id)
            num_max -= 1
        # nid = num_max // 3
        # nt = num_max // 3
        # na = num_max - nt - nid
        # pid = [None] * nid
        # pt = [None] * nt
        # pa = [None] * na
        best_id = None
        best_t = None
        best_a = None
        p_id = None
        p_t = None
        p_a = None
        for paper in self.papers:

            dist_id = textdistance.hamming.distance(query_paper.id, paper.id)
            if best_id is None or dist_id < best_id:
                if paper.id != equal_id.id:
                    best_id, p_id = dist_id, paper

            dist_a = textdistance.jaccard.distance(query_paper.authors, paper.authors)
            if best_a is None or dist_a < best_a:
                best_a, p_a = dist_a, paper

            dist_t = textdistance.jaccard.distance(query_paper.title, paper.title)
            if best_t is None or dist_t < best_t:
                best_t, p_t = dist_t, paper

        similar_papers += [p_id, p_a, p_t]
        return similar_papers

def get_bibtex_field(field, bibtex):
    # # bibtex = bibtex.replace("\n","")
    # # [res] = re.findall(r"[^\w]" + field + r".*=[^=\s]*\{([^=.]*)\}\,", bibtex)
    # # kv = re.compile(r'\b(?P<key>\w+)\s*=\s*{(?P<value>[^}]+)}')
    # kv = re.compile(r'\b(?P<key>\w+)={(?P<value>[^}]+)}')
    # res = kv.findall(bibtex)
    # # print("{}:".format(field), res)
    # return dict(res).pop(field, None)
    r1 = re.sub(r'.*[^a-zA-Z]' + field+r'\s*=\s*{', '', bibtex)
    ct_open = 1
    i = 0
    while ct_open > 0:
        if i >= len(r1):
            raise KeyError
        if r1[i] == "}":
            ct_open -= 1
        elif r1[i] == "{":
            ct_open += 1
        i += 1
        # print(ct_open, r1[:i])

    res = r1[:i-1]
    return None if len(res) == 0 else res

def get_headline_from_bibtex(bibtex):
    bibtex = bibtex.replace("\n","")
    # headline = re.findall(r"\@\w+\{\n*[\w\d]*\n*\,", bibtex)[0]
    [headline] = re.findall(r"\@\w+\{[\w\d]*\,", bibtex)
    return headline

def get_id_from_bibtex(bibtex):
    bibtex = bibtex.replace("\n","")
    # ids = re.findall(r"\@\w+\{\n*([\w\d]*)\n*\,", bibtex)
    ids = re.findall(r"\@\w+\{([\w\d]*)\,", bibtex)
    if ids is None or len(ids) == 0:
        bib_id = datetime.now().strftime("pan%Y%m%dh%H%Ms%S")
    else:
        bib_id = ids[0]
    return bib_id

def set_id_in_bibtex(bib_id, bibtex):
    new_bib = re.sub(r"\@([\w]+)\{([\w\d]*)\,", r"@\1{{{},".format(bib_id), bibtex)
    return new_bib

def clean_bibtex(bibtex):
    # res = bibtex.replace("\t", " ")
    # res = bibtex.replace("\n", " ")
    res = re.sub(r"[\t\n]", " ", bibtex)
    res = re.sub(r"\s+", " ", res)
    res = re.sub("\s*=\s*{", "={", res)
    return res
