import re
import glob
from tqdm import tqdm
from fuzzywuzzy import fuzz

def _clean_string_for_match(s):
    s = re.sub(r'[\_\.]', ' ', s)
    return s.lower()

def match(papers, paper_folders):

    if isinstance(paper_folders, str):
        paper_folders = [paper_folders]
    local_papers = {}
    duplicates = {}
    for pf in paper_folders:
        for l in glob.iglob(pf + "/**/*.pdf", recursive=True):
            l_name = l.split("/")[-1]
            # check for duplicated filenames
            if l_name in local_papers.keys():
                if l_name not in duplicates.keys():
                    duplicates[l_name] = [local_papers[l_name]] 
                duplicates[l_name].append(l)

            # store filenames and paths
            local_papers[l_name] = l

    print(f"I found {len(duplicates)} duplicated files")
    for p_name_, p_list_ in duplicates.items():
        print(p_name_)
        for p_ in p_list_:
            print(f"\t{p_}")

    local_papers_tmp = local_papers.copy()
    papers_not_matched = local_papers.copy()

    paper_locations = {}
    tqdm_bar = tqdm(papers)
    for p in tqdm_bar:
        #query:
        # - serach
        m_val, m_val_2 = 0, 0

        res = {}
        for k, v in local_papers_tmp.items():
            res[k] = fuzz.ratio(_clean_string_for_match(p.title), _clean_string_for_match(k))
        sorted_keys = sorted(res, key=lambda k: res[k], reverse=True)
        # print(res[sorted_keys[0]])
        found = False
        if res[sorted_keys[0]] > 90:
            found = True
        if res[sorted_keys[0]] > 60:
            found = True
            if sorted_keys[0] in list(paper_locations.values()):
                print("Warning with ", sorted_keys[0], "at", res[sorted_keys[0]])
            try:
                year = float(re.findall(r"(\d\d\d\d)", sorted_keys[0]))
                import pan
                year2 = float(pan.get_bibtex_field(bibtex=p.bibtex, field="year"))
                if not (year -1 <= year2 and year2 <= year + 1):
                    found = False
            except:
                pass

        if found:
            paper_locations[p.id] = (local_papers[sorted_keys[0]], res[sorted_keys[0]])
            if res[sorted_keys[0]] > 90:
                local_papers_tmp.pop(sorted_keys[0])

            if res[sorted_keys[0]] > 60:
                papers_not_matched.pop(sorted_keys[0], False)
                tqdm_bar.set_description(f"Rem. pdf: {len(papers_not_matched)}/{len(local_papers)}")

    print(f"I couln't match {len(papers_not_matched)} papers from the filesystem")

    from collections import Counter
    print(f"Multiple assignements:")
    for k, v in Counter([p_[0] for _, p_ in paper_locations.items()]).items():
        if v > 1:
            print(f"{k}: {v}")   


    return paper_locations, papers_not_matched
