import pan
from pan import utils


class UserInterface(object):

    def create_new_paper(self):
        raise NotImplemented

    def popup(self, msg):
        raise NotImplemented

    def view_paper(self, paper):
        raise NotImplemented

    def edit_paper(self, paper):
        raise NotImplemented


class EditorCLI(UserInterface):
    
    def create_new_paper(self):
        bib = utils.read_from_editor("Bibtex")
        bib = bib
        # comment = utils.read_from_editor("Comment")
        comment = ""
        # bib = """
        #     @article{adams2007bayesian,
        #     title={Bayesian online changepoint detection},
        #     author={Adams, Ryan Prescott and MacKay, David JC},
        #     journal={arXiv preprint arXiv:0710.3742},
        #     year={2007}
        #     }
        #     """
        # comment = "provaaa"
        headline = pan.get_headline_from_bibtex(bibtex=bib)
        bib_id = pan.get_id_from_bibtex(bibtex=bib)
        print("from headline:\n>{}\nretrieved id\n>{}<".format(headline, bib_id))
        print("want to change id?")
        resp = utils.yes_no_all_quit_input(toall=False, default=1)
        if resp == utils.YES:
            bib_id = input("type the new id: ")
            bib = pan.set_id_in_bibtex(bib_id=bib_id, bibtex=bib)
    
    
        paper = pan.Paper(paper_id=bib_id, bibtex=bib, comment=comment)
        return paper
    
    def popup(self, msg):
        print()
        return input(msg)

    def view_paper(self, paper):
        # paper = self.get(paper)
        form = [("field", ["id", paper.id]),
                ("comment", " author: {}".format(paper.author)),
                ("comment", " title: {}".format(paper.title)),
                ("field", ["bibtex", paper.bibtex]),
                ("field", ["doi", paper.doi]),
                ("field", ["official_url", paper.official_url]),
                ("field", ["link_preprint", paper.link_preprint]),
                ("field", ["date_creation", paper.date_creation]),
                ("field", ["last_update", paper.last_update]),
                ("field", ["repository", paper.repository]),
                ("field", ["experiment_results", paper.experiment_results]),
                ("field", ["order", paper.order]),
                ("field", ["keywords", paper.keywords]),
                ("field", ["comment", paper.comment + "\n"])]
        return utils.editor_form(form)
        # print(edited_vals)

    def edit_paper(self, paper):
        editions = self.view_paper(paper)[1:]
        yes_all = False
        confirmed_editions = []
        for e in editions:
            if e[1] == str(paper.__getattribute__(e[0])):
                pass
            else:
                print(utils.fs.bold, "Please confirm the below edition:", utils.fs.reset)
                print(e[0], ")",
                      utils.fg.red, "\t", paper.__getattribute__(e[0]), utils.fs.reset,
                      "-->",
                      utils.fg.green, e[1], utils.fs.reset)
                if yes_all:
                    res = utils.YES
                else:
                    res = utils.yes_no_all_quit_input(toall=True, default=0)
                
                if res == utils.ALL:
                    yes_all = True
                    res = utils.YES

                if res == utils.YES:
                    print("apply")
                    confirmed_editions.append(e)
                elif res == utils.NO:
                    print("pass... not edit")
                elif res == utils.QUIT:
                    print("quitting all..")
                    break
        return confirmed_editions


def debug():
    db = pan.PaperDataBase()
    ui = EditorCLI()

    paper_id = "pan20200325h1943s57"
    # pan.Paper.from_json_entry(db.papers[0])

    # confirmed_edits = ui.edit_paper(db.get(paper_id))
    confirmed_edits = [("id", "caccamo"),
                       ("bibtex", "@article{caccamo_v3,\n  title={titolp},\n  author={autori},\n  journal={jj},\n  volume={71},\n  pages={216--229},\n  year={2017},\n  publisher={Elsevier}\n}")]
    # new_paper, any_edit = ui.edit_paper(pan.Paper.from_json_entry(db.papers[0]))

    if len(confirmed_edits)>0:
        db.update(paper_id=paper_id, edits=confirmed_edits)
        db.save()
