# PAn: Paper Annotation dabatabase

## Commands

Check out `python scripts/run.py --help`


## Install

* install `generate-md`: `npm install -g markdown-styles`.
* configure config file `scripts/config.py`
* move to the pan directory and install it `python -m pip install -e .`
