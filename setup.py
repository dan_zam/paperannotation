from distutils.core import setup

setup(
    name='pan',
    version='0.1.0',
    packages=['pan',],
    license='BSD-3-Clause',
    author='Daniele Zambon',
    author_email='daniele.zambon.m@gmail.com',
    #description=('...'),
    long_description=open('README.md').read(),
    install_requires=['textdistance', 'markdown'],
    #url='https://github.com/...',
)
