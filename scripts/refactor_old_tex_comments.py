import pan
import re
import datetime
import bibtexparser

REVIEW_END = "REVIEWEND"
REVIEW_BEGIN = "REVIEWBEGIN"
REVIEW_SEP = REVIEW_END + "€" + REVIEW_BEGIN

def split_papers(paper_tex, path):
    
    re_sec = re.compile(r"\\[(sub)]*section{(?P<date>[^}]+)}")

    files = []
    with open(path+paper_tex, "r") as f:
        new_file = paper_tex + "__default.tex"
        fout = open(path+"out_tex/"+new_file, "w")
        files.append(new_file)
        
        for line in f:
            sec_title = re_sec.findall(line)
            if len(sec_title) > 0:
                assert len(sec_title) == 1
                if fout is not None:
                    fout.close()
                title = sec_title[0].replace(" ", "-")
                print(title)
                new_file = paper_tex + "__" + title + ".tex"
                new_file = new_file.replace(" ", "-")
                fout = open(path+"out_tex/"+new_file, "w")
                files.append(new_file)
            else:
                fout.write(line)

    return files


def refactor(paper_tex, paper_bib, pan_json, keywords):
    
    # split
    re_split = re.compile(REVIEW_BEGIN + r"{(?P<date>[\d/]+)}{(?P<bibid>[\w\d]+)}(?P<cmt>[^€]+)" + REVIEW_END)
    re_sec = re.compile(r"\\[(sub)]*section")
    
    with open(paper_tex, "r") as f:

        # read file
        text = str(f.read())
    
        # remove crap
        text = text.replace("$\leq$jul17", "01/01/2016")
    
        # final checks
        ff = re_sec.findall(text)
        assert len(ff) == 0, str(ff)
    
        # detectable papers
        text = text.replace("\\pnPaperReview", REVIEW_SEP) + REVIEW_SEP
    
        res = re_split.findall(text)
    
    # raise ValueError
    db = pan.PaperDataBase(filename=pan_json).init_empty()
    
    wri = bibtexparser.bwriter.BibTexWriter()
    pap = []
    with open(paper_bib, "rb") as f:
        bib_database = bibtexparser.load(f)
        for r in res:
            # print("\n\n\n---------------------------------------------------------\n\n\n")
            # print(r[0])
            # print(wri._entry_to_bibtex(bib_database.entries_dict[r[1]]))
            # print(r[2])
            
            # dictionary to bibtex string
            bib = wri._entry_to_bibtex(bib_database.entries_dict[r[1]])
            bib = re.sub(r"\s*=\s*{", "={", bib)
            assert r[1] == pan.get_id_from_bibtex(bibtex=bib)

            pap.append(pan.Paper(paper_id=r[1],
                                 bibtex=bib,
                                 comment=r[2],
                                 date_creation=datetime.datetime.strptime(r[0], '%d/%m/%Y').strftime("%Y-%m-%d"),
                                 last_update="2020-05-01",
                                 keywords=keywords))
            db.add(pap[-1])
    
    db.save()
    return db


if __name__ == "__main__":
    
    # base path
    path = "../"
    out_json = "out_json/"
    out_tex = "out_tex/"
    # original papers.tex with comments
    paper_tex = "papers.tex"
    # reference bibtex
    p_bib = path + "DZnoduplicate.bib"
    # aggregated db
    aggregated_json = path+ out_json + "papers_aggregated.json"
    import os
    if not os.path.exists(path + out_json):
        os.makedirs(path + out_json)
    if not os.path.exists(path + out_tex):
        os.makedirs(path + out_tex)

    # split the papers.tex into (sub)sections
    sec_tex = split_papers(paper_tex, path)

    # hard coded list of (sub)sections files to add keywords
    p_list = [
                ("papers.tex__Change-Detection.tex", "cd"),
                ("papers.tex__Competitive-methods.tex", "none"),
                ("papers.tex__Complex-networks-and-generative-models.tex", "complex networks"),
                ("papers.tex__default.tex", "none"),
                ("papers.tex__Differential-Geometry.tex", "geometry"),
                ("papers.tex__Graph-Dissimilarities.tex", "distance"),
                ("papers.tex__Graph-Embeddings.tex", "embedding"),
                ("papers.tex__Graph-features.tex", "features"),
                ("papers.tex__Graph-Kernels.tex", "kernel"),
                ("papers.tex__Graph-Neural-Networks.tex", "gnn"),
                ("papers.tex__Graph-Signal-Processing-and-Graph-Properties.tex", "gsp"),
                ("papers.tex__Information-Theory.tex", "it"),
                ("papers.tex__Kernel-adaptive-filtering.tex", "kaf"),
                ("papers.tex__Non-Stationarity.tex", "stationarity"),
                ("papers.tex__Others.tex", "none"),
                ("papers.tex__Signal-processing.tex", "sp"),
                ("papers.tex__Signal-to-graphs.tex", "signal to graph"),
                ("papers.tex__Statistical-Inference-and-Tests.tex", "stats"),
                ("papers.tex__Stochastic-Graph-Process.tex", "graph processes"),
                ("papers.tex__Vertex-Embeddings.tex", "node embedding")
            ]

    # check consistency
    for p_tex, kw in p_list:
        assert p_tex in sec_tex, p_tex + " not in sec_tex: " + str(sec_tex)
    for st in sec_tex:
        assert st in [p for p, _ in p_list], st + " not in p_list: " + str(p_list)

    try:
        db_agg = pan.PaperDataBase(filename=aggregated_json).init_empty()
    except FileExistsError:
        import os
        os.remove(aggregated_json)
        db_agg = pan.PaperDataBase(filename=aggregated_json).init_empty()
    # merge with current
    import config
    db_current = pan.PaperDataBase(filename=config.database_file[:-5] + " (copy).json")

    db_agg.add(db_current.get_all())

    # genereta json form sec_texs
    for p_tex, kw in p_list:
        p_json = path + out_json + p_tex + ".json"
        p_tex = path + out_tex + p_tex
        print("refactoring", p_tex)
        try:
            res = refactor(paper_tex=p_tex, paper_bib=p_bib, pan_json=p_json, keywords=kw)
            print("... found {} papers".format(len(res.papers)))
        except KeyError as e:
            import os
            os.remove(p_json)
            raise e
        except FileExistsError:
            print("FileExistsError but passing...")
            pass
        db_tmp = pan.PaperDataBase(p_json)
        db_agg.add(db_tmp.get_all())
    
    # save
    db_agg.save()
    
    