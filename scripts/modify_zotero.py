import sqlite3
from sqlite3 import Error
import re
from tqdm import tqdm


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def get_itemid_from_title(conn, title):
    cur = conn.cursor()
    title_ = title.lower()
    title_ = re.sub(r"\-\-", "–", title_)
    title_ = re.sub(r"\{\\\'e\}", "é", title_)
    title_ = re.sub(r"[\{\}\\\$]", "", title_)
    cur.execute(f"SELECT itemID FROM itemData WHERE valueID = "
                f"(SELECT valueID FROM itemDataValues WHERE value = \"{title_}\" COLLATE NOCASE)")
    rows = cur.fetchall()
    return [r[0] for r in rows]


def ensure_single_entry(conn, papers):
    cur = conn.cursor()
    problematic_papers = []
    for paper in tqdm(papers, desc="check duplicates"):
        itemid = get_itemid_from_title(conn, paper.title)
        if len(itemid) != 1:
            print(paper.id, itemid, paper.title)
            get_itemid_from_title(conn, paper.title)
            problematic_papers.append((paper.id, itemid))

    return problematic_papers


def update_note_entry(conn, papers):
    cur = conn.cursor()
    for paper in tqdm(papers, desc="update notes"):
        itemid = get_itemid_from_title(conn, paper.title)
        if len(itemid) != 1:
            print(f"Skipping paper with id {itemid}\n{paper}")
        # new_note = paper.dumps_comment_html()
        new_note = paper.comment
        # new_note = f"<div class=\"zotero-note znv1\">{new_note}</div>" <div data-schema-version="8">
        # new_note = f'<div data-schema-version="8">{new_note}</div>'
        new_note = re.sub(r"\'", "''", new_note)
        new_note = re.sub(r"\"", "\\\"", new_note)
        # new_note = re.sub(r"\#", "\\#", new_note)
        cur.execute(f"SELECT itemID FROM itemNotes WHERE parentItemID = {itemid[0]}")
        rows = cur.fetchall()
        if len(rows) != 1:
            print(f"Skipping with id {itemid} and notes {rows}:\n{paper}")
        else:
            cur.execute(f"UPDATE itemNotes SET note = \'{new_note}\' "
                        f"WHERE parentItemID = {itemid[0]}")
            rows = cur.fetchall()


def import_html_notes(papers, database):
    # create a database connection
    conn = create_connection(database)
    with conn:
        problematic_papers = ensure_single_entry(conn, papers)
        if len(problematic_papers) > 0:
            print("Problematic papers:")
        for p in problematic_papers:
            print(p)

        # update_note_entry(conn, papers)


def export_bibtex(dates, fbib, fcsv, db):

    from datetime import datetime
    from_, to_ = dates.split("-")
    papers = db.get_from_creationdate(
        from_date=datetime.strptime(from_, '%d/%m/%Y').date(),
        to_date=datetime.strptime(to_, '%d/%m/%Y').date()
    )

    with open(fbib, "w") as f:
        for paper in tqdm(papers, desc="write bibtex"):
            f.write(paper.dumps_extended_bibtex_for_zotero(
                additional_fields={
                    "note": "title",
                    "tags": "keywords",
                    "extra": {
                        "Pan creation": "date_creation",
                        "Pan last update": "last_update",
                        "DOI": "doi",
                        "Pan Official URL": "_official_url",
                        "Pan Preprint": "link_preprint",
                        "Repository": "repository",
                        "Official_url": "_official_url",
                        "Experiments": "experiment_results",
                    },
                })
            )
            f.write("\n")

    with open(fcsv, "w") as f:
        ids = []
        for paper in tqdm(papers, desc="write notes"):
            if paper.comment != "":
                f.write(f"{paper.id}, {paper.dumps_comment_html()};")
                f.write("\n")
                ids.append(paper.id)

    counts = {}
    for id in ids:
        c = ids.count(id)
        if c != 1:
            counts[id] = c

    if len(counts) > 0:
        print("Duplicated ids")
    for id, c in counts.items():
        print(id, c)

if __name__ == "__main__":
    import config
    import csv
    import pan

    fase = "generate"
    # fase = "update_notes"

    dates = "01/01/2020-1/1/2022"
    base_name = "my_pan_notes"

    zotero_db = config.zotero_db

    db = pan.PaperDataBase(filename=config.database_file)
    fbib = base_name + "_zotero.bib"
    fcsv = base_name + "_notes_zotero.csv"

    if fase == "generate":
        export_bibtex(dates, fbib, fcsv, db)
        print(f"Now:")
        print(f" - import {fbib} into Zotero")
        print(f" - unpin all citation keys")
        print(f" - close Zotero")
        print(f" - run this script with <fase=\"update_notes\">")

    elif fase == "update_notes":
        with open(fcsv, 'r') as file:
            csvreader = csv.reader(file)
            ids = [row[0] for row in csvreader]
        papers = [db.get(id) for id in ids]
        # papers = [p for p in papers if p is not None]
        import_html_notes(database=zotero_db, papers=papers)