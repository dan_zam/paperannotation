import argparse, os
import pan
import config

parser = argparse.ArgumentParser(description='Report the difference in annotations.')
parser.add_argument('--new', '-n', action='store_true',
                    help='Add new paper.')
parser.add_argument('--edit', '-e', nargs=1, action='store', default=False,
                    help='Edit paper .')
parser.add_argument('--view', '-v', nargs=1, action='store', default=False,
                    help='Prints out the specified entry.')
parser.add_argument('--last-read', '-lr', type=int, action='store',
                    help='List the last read papers. Pass an integer specifying how many to display.')
parser.add_argument('--report', action='store_true',
                    help='Create report.')
parser.add_argument('--search', '-s', nargs=1, action='store', default=False,
                    help='Search in all fields.')
parser.add_argument('--search-id', '-si', nargs=1, action='store', default=False,
                    help='Search within the ids.')
parser.add_argument('--search-author', '-sa', nargs=1, action='store', default=False,
                    help='Search within the authors.')
parser.add_argument('--search-title', '-st', nargs=1, action='store', default=False,
                    help='Search within the title.')
parser.add_argument('--bibtex', '-b', nargs="+", action='store', default=False,
                    help='Print bibtex.')
parser.add_argument('--generate-bibtex', type=str, default=False,
                    help='Print bibtex of from a latex .aux file (if provided), otherwise of all stored papers.')
parser.add_argument('--generate-yaml', type=str, default=False,
                    help='Print a yaml file of papers from the specified author.')
parser.add_argument('--from-author', action='store_true', default=False)
parser.add_argument('--from-latex-aux', action='store_true', default=False)

parser.add_argument('--update-local-papers', action='store_true',
                    help='Create report.')

parser.add_argument('--diff', nargs="+",
                    help='Compares two pan .json. File can be local or retrieved from a git commit.')


if __name__ == "__main__":

    args = parser.parse_args()
    ui = pan.interfaces.EditorCLI()

    if not os.path.isfile(config.database_file):
        ui.popup("File {} does not exist... press any key to terminate.".format(config.database_file))
    db = pan.PaperDataBase(filename=config.database_file)
    # print(f"Reading from pan database {config.database_file}")
    # db = pan.PaperDataBase()


    ### --- Edit and view papers ------------------------------------------- ###

    if args.new:
        new_paper = ui.create_new_paper()
        if not db.has_paper(new_paper):
            db.add(new_paper)
            db.save()
            print("The following paper has be added to the database.")
        else:
            ui.popup("Paper already present... press any key to terminate.")

        print(":ID:")
        print(new_paper.id)
        print(":BIBTEX:")
        print(new_paper.bibtex)
        print(":COMMENT:")
        print(new_paper.comment)
        args.edit = [new_paper.id]

    if args.edit:
        paper_id = args.edit[0]
        paper = db.get(paper_id)
        if paper is None:
            args.search = [paper_id]
        else:
            confirmed_edits = ui.edit_paper(paper)
            # confirmed_edits = [("id", "caccamo")]
            # new_paper, any_edit = ui.edit_paper(pan.Paper.from_json_entry(db.papers[0]))
            if len(confirmed_edits) > 0:
                db.update(paper_id=args.edit[0], edits=confirmed_edits)
                db.save()

    if args.view:
        p = db.get(args.view[0])
        print()
        print(p.to_string(color=True, compact=False, comment=True))
        print()
        # ui.view_paper(p)

    if args.bibtex:
        for paper_id in args.bibtex:
            print()
            print(db.get(paper_id).dumps_bibtex())



    ### --- Search papers ------------------------------------------- ###

    if args.search:
        args.search_author = args.search
        args.search_id = args.search
        args.search_title = args.search

    _something_to_seach = args.search or args.search_id or args.search_author or args.search_title

    if _something_to_seach:
        paper_found = {}
 
 
    if args.search_author:
        lll = db.query_author(query=args.search_author[0])
        print(" --- Seach Authors --- ")
        for l in lll:
            print(db.get(l).to_string(color=True, compact=False))
        paper_found["author"] = lll
        
    if args.search_id:
        lll = db.query_id(query=args.search_id[0])
        print(" --- Search ID --- ")
        for l in lll:
            print(db.get(l).to_string(color=True, compact=False))
        paper_found["id"] = lll

    if args.search_title:
        lll = db.query_title(query=args.search_title[0])
        print(" --- Search Title --- ")
        for l in lll:
            print(db.get(l).to_string(color=True, compact=False))
        paper_found["title"] = lll

    if _something_to_seach:
        print(" --- Recap --- ")
        for k, list_v in paper_found.items():
            print("Key:", k)
            print(end="  ")
            for v in list_v:
                print(v, end=" ")
            print()


    ### --- Reports papers ------------------------------------------- ###

    if args.generate_bibtex or args.generate_yaml:
        if args.generate_bibtex and args.generate_yaml:
            raise ValueError("Please decide either yaml or a bibtex")
        elif args.generate_bibtex:
            query = args.generate_bibtex
        elif args.generate_yaml:
            query = args.generate_yaml

        if args.from_author:
            if isinstance(query, str):
                papers = db.get_from_author(query)
                papers_notfound = []
            else:
                raise NotImplementedError()

        elif args.from_latex_aux:
            if isinstance(query, str):
                papers, papers_notfound = db.get_from_latex_aux(query)
            else:
                papers = db.get_all()
                papers_notfound = []
        else:
            raise ValueError("Please specify either --get-from-latex-aux or --get-from-author")

        if args.generate_bibtex:
            for paper in papers:
                print()
                print(paper.dumps_bibtex())

            if len(papers_notfound) > 0:
                print()
                print("# ------------------------------------")
                print("# The following papers were not found:")
                for p in papers_notfound:
                    print("#    ", p)

        if args.generate_yaml:
            papers_sorted = sorted(papers, key=lambda p: f"{p.year}{p.order:011.5f}{p.id}", reverse=True)
            for paper in papers_sorted:
                if paper.order < 0:
                    pass
                else:
                    print(" - ", end="")
                    print(paper.dumps_yaml())


    if args.last_read:
        num_to_show = args.last_read 
        num_expanded = 6 # the most recent "num_expanded" will have a more detailed description

        papers = db.get_all(sort="last_update")
        if num_to_show < 0:
            num_to_show = len(papers)
        for paper in papers[-num_to_show:-num_expanded]:
            print(paper.to_string(color=True, compact=True))
        for paper in papers[-min([num_to_show, num_expanded]):]:
            print(paper.to_string(color=True, compact=False))


    if args.update_local_papers:
        import pan.pdf_papers
        import pan.reports
        paper_locations, papers_not_matched = pan.pdf_papers.match(db.get_all(), config.paper_folder)
        pan.reports.save_paper_locations(paper_locations=paper_locations, filename=config.paper_location_file)
        if len(papers_not_matched) > 0:
            unmatched_papers_file = os.path.join(os.path.dirname(config.paper_location_file), "unmatched_papers.txt")
            with open(unmatched_papers_file, "w") as f:
                for fn_, path_ in papers_not_matched.items():
                    f.writelines(path_ + "\n")
            print(f"written unmatched papers to file {unmatched_papers_file}")

    if args.report:
        import pan.reports
        # report = pan.reports.MarkDownReport(path=config.report_folder, filename="report.md")
        # report = pan.reports.SimpleHTMLReport(path=".", filename="report_t.html")
        folders = config.report_folder
        paper_locations = pan.reports.load_paper_locations(filename=config.paper_location_file)
        if isinstance(config.report_folder, list):
            folders = config.report_folder
        else:
            folders = [config.report_folder]  
        for folder in folders:
            print("\n# Creating report for folder:", folder)
            report = pan.reports.MarkDown2HTMLReport(path=folder, filename="report.html",
                                                     paper_locations=paper_locations)
            report.generate(db)

    if args.diff:
        import json, subprocess
        import dictdiffer

        assert len(args.diff) == 2
        data = []
        for f in args.diff:
            if ":" in f:
                data.append(json.loads(subprocess.check_output(f"git cat-file -p {f}", shell=True)))
            else:
                data.append(json.load(open(f, "r")))

        differences = dictdiffer.diff(data[0], data[1])
        difference_list = list(differences)
        print("Pan differences:")
        difference_summary = {}
        difference_papers = {}
        for d in difference_list:
            difference_summary[d[0]] = difference_summary.get(d[0], 0) + 1
            if not d[0] in difference_papers.keys():
                difference_papers[d[0]] = []
            if d[0] == "change":
                p = data[0]["papers"][d[1][1]]["id"]
                print(f"\n{d[0]}\t{p}\t{d[1]}\n{d[2]}")
                difference_papers["change"].append(p)
            else:
                for el in d[2]:
                    p = el[1]["id"]
                    print(f'\n{d[0]}\t{d[1]}: {p}\n{el}')
                    difference_papers[d[0]].append(p)
                difference_summary[d[0]] += len(el) - 1

        print()
        print("Summary:")
        # print(difference_summary)
        for k, v in difference_papers.items():
            print(f'{k}[{len(v)}] {", ".join(v)}')
        # print(difference_papers)
