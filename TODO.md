# Bugs
- [ ] probable bug when you create a new entry from a bibtex and the bibtex has spacess between @article{ and the ID
- [ ] some scholar ids for bibtex are not in the form `[\w]+\d\d\d\d[\w]+`, for instance `serviansky2020set2graph`. make some checks at creation time to be compliant throughout

# Fields
- [ ] render latex in report display, such as `Fr\'{e}chet` 
- [ ] add keywords from a suggested set

# Features
- [x] given a tex project, read the `main.aux` and extract only the required references, and pass the file to `--generate-bibtex`
- [ ] flag to open the report
- [x] view entry instead of open vim for edit
- [x] search given keywords
- [ ] move --diff operations inside pan and maybe without dictdiffer package
- [ ] search engine in javascript  
- [ ] use more sophisticated checks for existence of papers
- [ ] consider using https://github.com/msiemens/tinydb
- [ ] automatic search for openreview files
- [ ] automatic search for local files (add base folder in config, and match names)
- [ ] retrieve num of publications
- [ ] search for keywords in html

